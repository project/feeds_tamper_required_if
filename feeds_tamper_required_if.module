<?php

/**
 * @file
 * Feeds Tamper - basic API functions and hook implementations.
 */

/**
 * Implements hook_ctools_plugin_directory().
 */
function feeds_tamper_required_if_ctools_plugin_directory($module, $plugin_type) {
  if ($module == 'feeds_tamper' && $plugin_type == 'plugins') {
    return 'plugins/' . $module;
  }
}

/**
 * Check to see if an item exists.
 * This function is the meat and potatoes of this module's two plugins.
 * It is meant to be called from within the plugin's callback function.
 *
 * We use the PHP 5.3 Reflection techniques here to alter some of the
 * protected methods and properties in Feeds classes.
 *
 * @see feeds_tamper_required_if_new_callback()
 * @see feeds_tamper_required_if_update_callback()
 */
function feeds_tamper_required_if_item_exists($source, $item_key) {

  // Start with an assumption that the item doesn't exist.
  $id = 0;

  // First check to see if the existingItemId() method exists on the processor.
  if (!method_exists($source->importer->processor, 'existingItemId')) {
    return $id;
  }

  // Clone the batch and set the current_item property on it.
  // We need to do this, because the methods called by existingItemId() need
  // to know what the current item is. However, that property is not set by Feeds
  // until the processing phase. So we figure it out now, and pass in a fake
  // $batch object with the current item set.
  $batch = clone $source->batch;
  $reflection = new ReflectionObject($batch);
  $current_item = $reflection->getProperty('current_item');
  $current_item->setAccessible(TRUE);
  $current_item->setValue($batch, $batch->items[$item_key]);

  // Invoke the existingItemId
  $method = new ReflectionMethod($source->importer->config['processor']['plugin_key'], 'existingItemId');
  $method->setAccessible(TRUE);
  $id = $method->invoke($source->importer->processor, $batch, $source);

  // Return the id.
  return $id;
}
