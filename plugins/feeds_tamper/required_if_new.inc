<?php

/**
 * @file
 * Make field required, but only if a new item is being created.
 * If not present, the feed item will be removed.
 * The field will not be required for existing items being updated.
 */

$plugin = array(
  'form' => 'feeds_tamper_required_if_new_form',
  'callback' => 'feeds_tamper_required_if_new_callback',
  'name' => 'Required if new',
  'multi' => 'direct',
  'category' => 'Required if...',
  'required' => TRUE,  // See http://drupal.org/node/2053135
);

function feeds_tamper_required_if_new_form($importer, $element_key, $settings) {
  $form = array();

  $form['help'] = array(
    '#value' => t('Make this field required for new items. If it is empty, the feed item will not be saved. This field will NOT be required for updating existing items.'),
  );

  // Error message.
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Error message'),
    '#description' => t('If the item is removed by this plugin, display the following error message.'),
    '#default_value' => !empty($settings['message']) ? $settings['message'] : '',
  );

  // Available replacement patterns for the message.
  $mappings = $importer->processor->config['mappings'];
  $replace = array();
  foreach ($mappings as $mapping) {
    $replace[] = '[' . strtolower($mapping['source']) . ']';
  }
  $form['replacements'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Replacement Patterns'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#value' => theme('item_list', $replace),
  );

  return $form;
}

function feeds_tamper_required_if_new_callback($source, $item_key, $element_key, &$field, $settings) {

  // See if the item already exists.
  $id = feeds_tamper_required_if_item_exists($source, $item_key);

  // If the item doesn't exist, and the field is empty, unset the item.
  if (empty($id) && empty($field)) {

    // If an error message is provided, print it.
    if (!empty($settings['message'])) {

      // If the item doesn't exist, bail. It's probably already been unset.
      if (!isset($source->batch->items[$item_key])) {
        return;
      }

      // Replace patterns in the message.
      $item = $source->batch->items[$item_key];
      foreach ($item as $key => $value) {
        $trans['[' . $key . ']'] = $value;
      }
      $message = strtr($settings['message'], $trans);
      drupal_set_message(filter_xss($message), 'error');
    }

    // Unset the item.
    unset($source->batch->items[$item_key]);
  }
}

