<?php

/**
 * @file
 * Make field required, but only if an existing item is being updated.
 * If not present, the feed item will be removed.
 * The field will not be required for creating new items.
 */

$plugin = array(
  'form' => 'feeds_tamper_required_if_update_form',
  'callback' => 'feeds_tamper_required_if_update_callback',
  'name' => 'Required if updating',
  'multi' => 'direct',
  'category' => 'Required if...',
  'required' => TRUE,  // See http://drupal.org/node/2053135
);

function feeds_tamper_required_if_update_form($importer, $element_key, $settings) {
  $form = array();

  $form['help'] = array(
    '#value' => t('Make this field required for existing items. If it is empty, the feed item will not be saved. This field will NOT be required for creating new items.'),
  );

  // Error message.
  $form['message'] = array(
    '#type' => 'textarea',
    '#title' => t('Error message'),
    '#description' => t('If the item is removed by this plugin, display the following error message.'),
    '#default_value' => !empty($settings['message']) ? $settings['message'] : '',
  );

  // Available replacement patterns for the message.
  $mappings = $importer->processor->config['mappings'];
  $replace = array();
  foreach ($mappings as $mapping) {
    $replace[] = '[' . strtolower($mapping['source']) . ']';
  }
  $form['replacements'] = array(
    '#type' => 'fieldset',
    '#title' => t('Available Replacement Patterns'),
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#value' => theme('item_list', $replace),
  );

  return $form;
}

function feeds_tamper_required_if_update_callback($source, $item_key, $element_key, &$field, $settings) {

  // Only perform this operation if the processor's update_existing config is
  // set to "Replace existing nodes" (1) or "Update existing nodes (slower than
  // replacing them)" (2).
  if ($source->importer->config['processor']['config']['update_existing'] == 0) {
    return;
  }

  // See if the item already exists.
  $id = feeds_tamper_required_if_item_exists($source, $item_key);

  // If the item exists, and the field is empty, unset the item.
  if (!empty($id) && empty($field)) {

    // If an error message is provided, print it.
    if (!empty($settings['message'])) {

      // If the item doesn't exist, bail. It's probably already been unset.
      if (!isset($source->batch->items[$item_key])) {
        return;
      }

      // Replace patterns in the message.
      $item = $source->batch->items[$item_key];
      foreach ($item as $key => $value) {
        $trans['[' . $key . ']'] = $value;
      }
      $message = strtr($settings['message'], $trans);
      drupal_set_message(filter_xss($message), 'error');
    }

    // Unset the item.
    unset($source->batch->items[$item_key]);
  }
}

